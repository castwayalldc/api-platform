<?php
/**
 * Created by PhpStorm.
 * User: castway
 * Date: 2019-09-11
 * Time: 11:26
 */

namespace App\Security;


use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\ExpiredTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Guard\JWTTokenAuthenticator;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class TokenAuthenticator extends JWTTokenAuthenticator
{
    public function getUser($preAuthToken, UserProviderInterface $userProvider)
    {
        /** @var User $user */
        $user = parent::getUser(
            $preAuthToken,
            $userProvider
        );

        var_dump($user->getPasswordChangeDate());
        var_dump($preAuthToken->getPayload()['iat']);

        if ($user->getPasswordChangeDate() && $preAuthToken->getPayload()['iat'] < $user->getPasswordChangeDate()){
            var_dump('sussie');
            throw new ExpiredTokenException();
        }

        return $user;
    }
}