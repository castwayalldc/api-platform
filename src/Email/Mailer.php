<?php
/**
 * Created by PhpStorm.
 * User: castway
 * Date: 2019-09-13
 * Time: 11:49
 */

namespace App\Email;

use App\Entity\User;
use Twig\Environment;

class Mailer
{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var Environment
     */
    private $twig;

    public function __construct(
        \Swift_Mailer $mailer,
        Environment $twig
    )
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function sendEmail(\Swift_Message $message)
    {
        return $this->mailer->send($message);
    }

    public function sendConfirmationEmail(User $user){
        $body = $this->twig->render(
            'email/confirmation.html.twig',
            [
                'user' => $user
            ]
        );

        $message = (new \Swift_Message('Please confirm your account!'))
            ->setFrom('castwaytyrr@gmail.com')
            ->setTo($user->getEmail())
            ->setBody($body, 'text/html')
        ;
        $this->sendEmail($message);
    }
}